package com.lunjan.ttl;

import android.app.Activity;
import android.content.SharedPreferences;
import java.lang.Process;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.DataOutputStream;
import android.os.Bundle;
import android.view.View.OnClickListener;


public class MainActivity extends Activity implements OnClickListener
{

	Button buttonLoad, buttonSave;
	TextView textBox1;
	
	private static final String PREFS_NAME = "MyPrefs";	
	ShellService shell = new ShellService();

	String getVal = "cat /proc/sys/net/ipv4/ip_default_ttl";
	public final String SU = "su";

    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
	
		textBox1 = (TextView) findViewById(R.id.wv);
		textBox1.setText("Ready...");

		buttonLoad = (Button) findViewById(R.id.loadFileButton);
		buttonSave = (Button) findViewById(R.id.saveFileButton);

		buttonLoad.setOnClickListener(this);
		buttonSave.setOnClickListener(this);		

    }

	public void onClick(View v) {		

		switch (v.getId())
		{
			case R.id.loadFileButton:
				{

					shell.runSU(shell.SU);
					String res = shell.readShell(getVal);
					textBox1.setText(getVal + "\n" + res);		
					break;
				}

			case R.id.saveFileButton:
				{
					String ttlValue = ((TextView)findViewById(R.id.editbox)).getText().toString();
					String putValue = "echo " + ttlValue + " > /proc/sys/net/ipv4/ip_default_ttl";
					shell.runSU(shell.SU);
					shell.runSU(putValue);
					SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("TTL_VALUE", ttlValue);
					editor.commit();
					Toast.makeText(getBaseContext(), "ttl: <ip_default_ttl> value applied and saved as "+ ttlValue ,
								Toast.LENGTH_SHORT).show();
					break;
				}
		}
    }
	

}
