package com.lunjan.ttl;


import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class OnBootReceiver extends BroadcastReceiver
{
	private static final String PREFS_NAME = "MyPrefs";	
	ShellService shell = new ShellService();

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()))
		{
            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);	
			String ttlValue  = settings.getString("TTL_VALUE", "64");		
			String putValue = "echo " + ttlValue + " > /proc/sys/net/ipv4/ip_default_ttl";
			shell.runSU(shell.SU);
			shell.runSU(putValue);
			Toast.makeText(context, "ttl: <ip_default_ttl> value is " + ttlValue + "  now!" , Toast.LENGTH_LONG).show();
			System.gc();
        }
    }
}
