package com.lunjan.ttl;

import android.app.Activity;
import android.content.SharedPreferences;
import java.lang.Process;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.DataOutputStream;
import android.os.Bundle;
import android.view.View.OnClickListener;

public class ShellService
{
public static final String SU = "su";

	public String readShell(String tst) {
		try
		{
			Process process = Runtime.getRuntime().exec(tst);
			BufferedReader reader = new BufferedReader(
				new InputStreamReader(process.getInputStream()));
			int read;
			char[] buffer = new char[1024];
			StringBuffer output = new StringBuffer();
			while ((read = reader.read(buffer)) > 0)
			{
				output.append(buffer, 0, read);
			}
			reader.close();

			process.waitFor();

			return output.toString();
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}

    }	

	public Process runSU(final String s) {
		Process process = null;
		try
		{
			process = Runtime.getRuntime().exec("su");
			final DataOutputStream toProcess = new DataOutputStream(
				process.getOutputStream());
			toProcess.writeBytes("exec " + s + "\n");
			toProcess.flush();
		}
		catch (final Exception e)
		{
			process = null;
		}
		return process;
	}
	

}
